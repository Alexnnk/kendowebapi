﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApiUsers2.Models
{
    public class User2DbInitializer : DropCreateDatabaseAlways<User2Context>
    {
        protected override void Seed(User2Context db)
        {
            db.Users.Add(new User() { Id = 1, DepartmentId = 2, UserName = "Ash" });
            db.Users.Add(new User() { Id = 2, DepartmentId = 1, UserName = "Ivan" });
            db.Users.Add(new User() { Id = 3, DepartmentId = 1, UserName = "Serge" });
            base.Seed(db);
        }
    }
}