﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApiDepartments.Models
{
    public class DepDbInitializer : DropCreateDatabaseAlways<DepartmentContext>
    {
        protected override void Seed(DepartmentContext db)
        {
            db.Departments.Add(new Department() { Id = 1, Title = "Departament_1" });
            db.Departments.Add(new Department() { Id = 2, Title = "Departament_2" });
            db.Departments.Add(new Department() { Id = 3, Title = "Departament_3" });
            base.Seed(db);
        }

    }
}