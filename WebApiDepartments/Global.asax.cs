﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using WebApiContrib.Formatting.Jsonp;
using WebApiDepartments.Models;

namespace WebApiDepartments
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configuration.AddJsonpFormatter();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            Database.SetInitializer(new DepDbInitializer());
        }
    }
}
